const rotate = require('./app');

test('rotate 90 clockwise len = 3', () => {
  const matrix = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
  ];
  const result = [
    [7,4,1],
    [8,5,2],
    [9,6,3],
  ];
  expect(rotate(matrix, 1)).toEqual(result);
});

test('rotate 90 clockwise len = 4', () => {
  const matrix = [
    ['a','b','c','d'],
    ['e','f','g','h'],
    ['i','j','k','l'],
    ['m','n','o','p']
  ];
  const result = [
    ['m','i','e','a'],
    ['n','j','f','b'],
    ['o','k','g','c'],
    ['p','l','h','d']
  ];
  expect(rotate(matrix, 1)).toEqual(result);
});


test('rotate 180 clockwise len = 3', () => {
  const matrix = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
  ];
  const result = [
    [9,8,7],
    [6,5,4],
    [3,2,1],
  ];
  expect(rotate(matrix, 2)).toEqual(result);
});

test('rotate 270 clockwise len = 3', () => {
  const matrix = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
  ];
  const result = [
    [3,6,9],
    [2,5,8],
    [1,4,7],
  ];
  expect(rotate(matrix, 3)).toEqual(result);
});