
  function rotate(matrix, k){
    let rotate = k % 4;
    if(rotate == 0) return matrix;

    var length = matrix.length
    for(var row = 0; row < (length / 2); row++){
      for(var col = row; col < ( length - 1 - row); col++){
        for(var k = 0; k< rotate;k++){
          var tmpVal = matrix[row][col];
          for(var i = 0; i < 4; i++){
            var rowSwap = col;
            var colSwap = (length - 1) - row;
            var poppedVal = matrix[rowSwap][colSwap];
            matrix[rowSwap][colSwap] = tmpVal;
            tmpVal = poppedVal;
            col = colSwap;
            row = rowSwap;
          }
        }

      }
    }
    return matrix
  }
  module.exports = rotate;